# прикрыли лавочку
после пары фоток, скачанных таким способом, теперь дают бан

## lightshot-downloader

баш скрипт для скачки всех картинок с сайта prnt.sc

### как работает эта штука?

Довольно просто. Перебирает все возможные варианты шестизначного набора букв и цифр в данном диапазоне. Однако, по этому адресу лежит не картинка, а хтмл страница с картикой. При помощи магии wget выбираем только картинки, и качаем их в папочку imgs.

### зачем?

Я делал тупо по приколу. Хотя кому-то эта штука может быть полезной. Даже выцепить конфиденциальную информацию кому-то удавалось.

### как использовать?
`bash downloader.bash`

Настройки диапазона мне было лень делать нормально, поэтому подкорректируй скриптик под себя.