#!/bin/bash

mkdir -p imgs
cd imgs

for c1 in {a..z} {1..9} # можешь выбрать другой диапазон
do
    for c2 in {a..z} {0..9}
    do
        for c3 in {a..z} {0..9}
        do
            for c4 in {a..z} {0..9}
            do
                for c5 in {a..z} {0..9}
                do
                    for c6 in {a..z} {0..9}
                    do
                        echo downloading prnt.sc/$c1$c2$c3$c4$c5$c6 image...
                        wget -A "*.png" --no-parent -l 1 -nd -nc -k -p -H -E -K https://prnt.sc/$c1$c2$c3$c4$c5$c6 &> /dev/null 
                        rm ./imgs/*.tmp*
                    done
                done
            done
        done
    done
done
